# app-zatos
![Z!](../img/websitezatos.PNG)
solución digital para seleccionar los mejores materiales disponibles par fabricación de calzado y marroqinería.

## Arquitectura Propuesta 
![Z!](../img/ArqZatosCloud.PNG)
## Documentación Usuario - Fabricante - Solicita Materiales
![Z!](../img/material.PNG)
El fabricante ingresa los materiales que necesita para farbicar su producto. 
## Documentación Usuario - Fabricante - Lista de Pedido  - Envia a Provedores 
![Z!](../img/listapedido.png)
Lista de Materiales para posterior pedido  (Zatos con IA Busca su pedido más apropiado según necesidad ).
